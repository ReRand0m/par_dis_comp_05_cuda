#include <iostream>

__global__
void cuda_reduction(float* input, float* res, unsigned int N) {
    for (int i = 0; i < N; ++i) {
        *res += input[i];
    }
}

int main() {
    const unsigned int N = 1 << 22;

    cudaSetDevice (6);

    float *in = new float[N];
    float res;
    for (int i = 0; i < N; ++i)
        in[i] = 1;

    float* gpu_in;
    float* gpu_res;
    cudaMalloc(&gpu_in, sizeof(float) * N);
    cudaMalloc(&gpu_res, sizeof(float));
    cudaMemcpy(gpu_in, in, sizeof(float) * N, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    cuda_reduction<<<1, 1>>>(gpu_in, gpu_res, N);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaMemcpy(&res, gpu_res, sizeof(float), cudaMemcpyDeviceToHost);

    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    std::cout << ms << "ms " << std::fixed << res << " " << (res == N) << std::endl;

    cudaFree(gpu_in);
    cudaFree(gpu_res);
    delete[] in;

    return 0;
}
