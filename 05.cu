#include <iostream>

#define BLOCK_SIZE 512

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line)
{
    if (code != cudaSuccess)
        std::cerr << "\"" << cudaGetErrorString(code) << "\" in file " << file << ":" << line << std::endl;
}

__device__ void warp_reduce(volatile float* sharedData, unsigned int tid) {
    sharedData[tid] += sharedData[tid + 32];
    sharedData[tid] += sharedData[tid + 16];
    sharedData[tid] += sharedData[tid +  8];
    sharedData[tid] += sharedData[tid +  4];
    sharedData[tid] += sharedData[tid +  2];
    sharedData[tid] += sharedData[tid +  1];
}

__global__
void cuda_reduction(float* input, float* res, unsigned int N) {
    unsigned int tid = threadIdx.x;
    unsigned int id = blockIdx.x * blockDim.x * 2 + threadIdx.x;
    __shared__ float sharedData[BLOCK_SIZE];

    sharedData[tid] = input[id] + input[id + blockDim.x];
    __syncthreads();

    for (int k = blockDim.x / 2; k > 32; k>>=1) {
        if (tid < k)
            sharedData[tid] += sharedData[tid + k];
        __syncthreads();
    }

    if (tid < 32)
    {
        warp_reduce(sharedData, tid);
    }

    if(tid == 0) res[blockIdx.x] = sharedData[0];
}

int main() {
    const unsigned int N = 1 << 22;
    const unsigned int blocks = ((N + (BLOCK_SIZE - 1)) / BLOCK_SIZE) / 2;

    cudaSetDevice (6);

    float *in = new float[N];
    float *res = new float[blocks];

    for (int i = 0; i < N; ++i) {
        in[i] = 1;
    }

    float* gpu_in;
    float* gpu_res;
    cudaMalloc(&gpu_in, sizeof(float) * N);
    cudaMalloc(&gpu_res, sizeof(float) * blocks);
    cudaMemcpy(gpu_in, in, sizeof(float) * N, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    cuda_reduction<<<blocks, BLOCK_SIZE>>>(gpu_in, gpu_res, N);
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaEventRecord(stop);

    gpuErrchk( cudaMemcpy(res, gpu_res, sizeof(float) * blocks, cudaMemcpyDeviceToHost) );
    cudaEventSynchronize(stop);

    for (int i = 1; i < blocks; ++i) {
        res[0] += res[i];
    }

    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    std::cout << ms << "ms " << std::fixed << res[0] << " " << (res[0] == N) << std::endl;

    cudaFree(gpu_in);
    cudaFree(gpu_res);
    delete[] in;
    delete[] res;

    return 0;
}
